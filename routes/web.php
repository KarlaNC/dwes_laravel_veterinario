<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('home');
});*/

Route::get('/', 'InicioController@getInicio');

/*Route::get('login', function () {
    return view('auth.login');
});

Route::get('logout', function () {
    return 'Logout usuario';
});*/

Route::get('mascotas', 'MascotasController@getTodas');

Route::get('mascotas/ver/{id}'/*Con id-nombre*/, 'MascotasController@getVer')->where('id','[0-9]+'); //Es mascotas.mostrar

//Route::get('mascotas/crear', 'MascotasController@getCrear');

Route::post('mascotas/crear', 'MascotasController@postCrear');

//Route::get('mascotas/editar/{id}', 'MascotasController@getEditar')->where('id','[0-9]+');

Route::post('mascotas/editar/{id}', 'MascotasController@postEditar');


// --- Se han añadido automaticamente 

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

// Añade un middleware de tipo grupo que aplique el filtro auth para proteger las rutas de crear y editar mascotas.

Route::group(['middleware' => 'auth'], function() {
	Route::get('mascotas/crear', 'MascotasController@getCrear');
	Route::get('mascotas/editar/{id}', 'MascotasController@getEditar')->where('id','[0-9]+');
});


// --ruta api

Route::post('api', 'SoapServerController@getServer');

Route::post('api/wsdl', 'SoapServerController@getWSDL');