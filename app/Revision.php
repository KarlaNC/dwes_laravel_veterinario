<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revision extends Model
{
    //
    protected $table = "revisiones"; //Para que en la BD busque revisiones y no revisions
}
