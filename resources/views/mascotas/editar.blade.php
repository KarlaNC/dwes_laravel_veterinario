@extends('layouts.master')

@section('titulo')
	Editar
@endsection

@section('contenido')

<div class="row">
  <div class="offset-md-3 col-md-6">
    <div class="card">
      <div class="card-header text-center">
        Añadir mascota
      </div>
    <div class="card-body" style="padding:30px">
  {{-- TODO: Abrir el formulario e indicar el método POST --}}
    <form action="{{ url('mascotas/editar') }}/{{$mascotaSeleccionada->id}}" method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      {{-- TODO: Protección contra CSRF --}}
        <div class="form-group">
          <label for="nombre">Nombre</label>
          <input type="text" name="nombre" id="nombre" class="form-control" value = {{ $mascotaSeleccionada->nombre }}>
        </div>
        <div class="form-group">
        {{-- TODO: Completa el input para la especie --}}
        <label for="especie">Especie</label>
          <input type="text" name="especie" id="especie" class="form-control" value = {{ $mascotaSeleccionada->especie }}>
        </div>
        <div class="form-group">
        {{-- TODO: Completa el input para la raza --}}
        <label for="raza">Raza</label>
          <input type="text" name="raza" id="raza" class="form-control" value = {{ $mascotaSeleccionada->raza }}>
        </div>
        <div class="form-group">
        {{-- TODO: Completa el input para la fecha de nacimiento --}}
        <label for="fechaNac">Fecha nacimiento</label>
          <input type="date" name="fechaNac" id="fechaNac" class="form-control" value = {{ $mascotaSeleccionada->fechaNacimiento }}>
        </div>
        <div class="form-group">
        {{-- TODO: Completa el input para el cliente --}}
        <label for="cliente">Cliente</label>

        <input type="textarea" name="cliente" id="cliente" class="form-control" value = {{ $mascotaSeleccionada->cliente }}>
        </div>
        <div class="form-group">
          <label for="historial">Historial</label>
          <textarea name="historial" id="historial" class="form-control" rows="3">{{ $mascotaSeleccionada->historial }} </textarea>
        </div>
        <div class="form-group">
        {{-- TODO: Completa el input para la imagen --}}
        <label for="photo">Imagen</label>
        <input name="photo" type="file" class="btn btn-outline-dark" />
        </div>
        <div class="form-group text-center">
          <button type="submit" class="btn btn-outline-dark" >Editar mascota</button>
          <!--<input type="submit" class="btn btn-outline-dark" >Añadir mascota</input>-->
        </div>
    </form>
    {{-- TODO: Cerrar formulario --}}
    </div>
    </div>
  </div>
</div>

@endsection