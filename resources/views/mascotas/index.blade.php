@extends('layouts.master')

@section('titulo')
	Index
@endsection

@section('contenido')

	@if (session('mensaje'))
		<div class="alert alert-success" role="alert">
		  {{ session('mensaje') }}
		</div>
	@endif

	<div class="row">
		@foreach( $arrayMascotas as $mascota )
			<div class="col-xs-12 col-sm-6 col-md-2 ">
				<a href="{{ url('/mascotas/ver' ) }}/{{$mascota->id}}">
				<img class="border border-light" src="{{ asset('assets/imagenes/')}}/{{ $mascota->imagen }}" style="height:200px"/>
				<div style="margin-left: 50px;">
					<h4 style="min-height:45px;margin:auto ">
						{{ $mascota->nombre }}
					</h4>
					</a>
					<p>{{ $mascota->especie }}</p>
					<p>{{ $mascota->fechaNacimiento }}</p>
				</div>
			</div>
		@endforeach
	</div>

@endsection