<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SoapServerController extends Controller
{
    //

    public function getServer() {

    }

    public function getWSDL() {
    	
    }
}

class VeterinarioWebService
{

 
	// ▪ login que recibe un correo electrónico y contraseña y comprueba si el
	// usuario existe o no

    public function login ($email, $pass)
    {
        // Obtenemos los datos del formulario
        // $data = [
        //     'username' => $email //Input::get('username'),
        //     'password' => $pass //Input::get('password')
        // ];
 
        // // Verificamos los datos
        // if (Auth::attempt($data, Input::get('remember'))) // Como segundo parámetro pasámos el checkbox para sabes si queremos recordar la contraseña
        // {
        //     // Si nuestros datos son correctos mostramos la página de inicio
        //     return Redirect::intended('/');
        // }
        // // Si los datos no son los correctos volvemos al login y mostramos un error
        // return Redirect::back()->with('error_message', 'Invalid data')->withInput();

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
    }
 	
 	// ▪ getMascota que devuelve una mascota pasando el id de una
	// determinada mascota

 	public function getMascota ($id_mascota)
 	{
 		$mascotaID = Mascota::findOrFail($id);
		//return view('mascotas.mostrar',  array('mascotaSeleccionada' => $mascotaID));
 	}

	// ▪ getMascotas que devuelve un array con todas las mascotas del
	// veterinario

	public function getMascotas ( $nombre_cliente )
	{
		// return App\Mascota[]
	}
	// ▪ getMascotasCliente que recibe el nombre de un cliente y devuelve un
	// array de todas sus mascotas
	// ▪ búsqueda que devuelve un array con las mascotas cuyo nombre (o
	// parte de él) coincida con la cadena pasada como parámetro
}

	// public function showLogin()
 //    {
 //        // Verificamos si hay sesión activa
 //        if (Auth::check())
 //        {
 //            // Si tenemos sesión activa mostrará la página de inicio
 //            return Redirect::to('/');
 //        }
 //        // Si no hay sesión activa mostramos el formulario
 //        return View::make('login');
 //    }

    // public function logOut()
    // {
    //     // Cerramos la sesión
    //     Auth::logout();
    //     // Volvemos al login y mostramos un mensaje indicando que se cerró la sesión
    //     return Redirect::to('login')->with('error_message', 'Logged out correctly');
    // }