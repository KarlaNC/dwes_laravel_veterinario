<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mascota;
use Illuminate\Support\Facades\Storage;

class MascotasController extends Controller
{

    public function getTodas(){
    	$mascotas = Mascota::all();
		 return view('mascotas.index', array('arrayMascotas' => $mascotas)); //Mejor siempre asi, siendo asociati.
		 //Hay que llamar igual al array en la vista
	}

    public function getVer($id){ //Con id-nombre
    	$mascotaID = Mascota::findOrFail($id);
		return view('mascotas.mostrar',  array('mascotaSeleccionada' => $mascotaID));
	}

	public function getCrear(){
		return view('mascotas.crear');
	}

	public function postCrear(Request $request){
		
		$mascota = new Mascota();

		$mascota->nombre = $request->nombre; 
		$mascota->especie =  $request->especie;
		$mascota->raza =  $request->raza;
		$mascota->fechaNacimiento =  $request->fechaNac; //Poner el nombre exacto del html
		$mascota->imagen =  $request->photo->store( "",'mascotas'); // 
		$mascota->historial =  $request->historial;
		$mascota->cliente =  $request->cliente;

		try {
			$mascota->save();
			return redirect("mascotas")->with("mensaje", "Creado con exito");
		} catch (Exception $ex) { //\Illuminate\Database\QueryException
			return redirect("mascotas")->with("mensaje", "Fallo al crear la mascota");
		}

		//return view('mascotas.crear');
	}

	public function getEditar($id){
		$mascotaID = Mascota::findOrFail($id);
		return view('mascotas.editar',  array('mascotaSeleccionada' => $mascotaID));
	}

	public function postEditar(Request $request , $id){ //Recibir el request importatnte

		$mascotaID = Mascota::findOrFail($id); //Otra opcion seria pasarle por aqui la id 

		$mascotaID->nombre = $request->nombre; 
		$mascotaID->especie =  $request->especie;
		$mascotaID->raza =  $request->raza;
		$mascotaID->fechaNacimiento =  $request->fechaNac;
		$mascotaID->historial =  $request->historial;
		$mascotaID->cliente =  $request->cliente;
		//Mirar si tiene o no img

		if(!empty($request->photo) && $request->photo->isValid()){ // $request->has('photo')
			//$mascotaID->imagen =  $request->photo->store( "",'mascotas');
			Storage::disk("mascotas")->delete($mascotaID->imagen);
			$mascotaID->imagen  = $request->photo->store("","mascotas");
		}
		
		try {
			$mascotaID->save();
			return redirect("mascotas")->with("mensaje", "Editado con exito");
		} catch (Exception $ex) { //\Illuminate\Database\QueryException
			return redirect("mascotas")->with("mensaje", "Fallo al editar la mascota");
		}

		/*return view('mascotas.editar',  array('mascotaSeleccionada' => $mascotaID));*/
	}

}
