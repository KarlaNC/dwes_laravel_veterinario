<?php

use App\User;
use App\Mascota;
use App\Revision;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    private $arrayMascotas = array(
		array(
			'nombre' => 'Kira',
			'especie' => 'Perro',
			'raza' => 'Samoyedo',
			'fechaNacimiento' => '2014-09-07',
			'imagen' => 'perro.jpg',
			'historial' => 'Vacunas al día',
			'cliente' => 'Iván'
		),
		array(
			'nombre' => 'Conejo',
			'especie' => 'Conejo',
			'raza' => 'Enano',
			'fechaNacimiento' => '2011-07-07',
			'imagen' => 'conejo.jpg',
			'historial' => 'Limado de dientes. Vacunas al día',
			'cliente' => 'Pepita'
		),
		array(
			'nombre' => 'Gato',
			'especie' => 'Gato',
			'raza' => 'Siamés',
			'fechaNacimiento' => '2016-05-16',
			'imagen' => 'gato.jpg',
			'historial' => 'Todo bien',
			'cliente' => 'Pepe'
		),
	);

    public function run()
    {
    	self::seedMascotas();
    	self::seedUser();
    	self::seedRevisiones();
    	$this->command->info('Tabla mascotas inicializada con datos');
        // $this->call(UsersTableSeeder::class);
    }

    private function seedMascotas(){
    	DB::table('mascotas')->delete();//Borrado de toda la tabla
    	
		foreach ($this->arrayMascotas as $mascota) {
			$m = new Mascota();
			$m->nombre = $mascota['nombre'];
			$m->especie = $mascota['especie'];
			$m->raza = $mascota['raza'];
			$m->fechaNacimiento = $mascota['fechaNacimiento'];
			$m->imagen = $mascota['imagen'];
			$m->historial = $mascota['historial'];
			$m->cliente = $mascota['cliente'];
			$m->save();
		}
    }

    private function seedUser(){
    	DB::table('users')->delete();//Borrado de toda la tabla

    	$user = new User();
		$user->name = "karla";
		$user->email = "karla@gmail.com";
		$user->password = bcrypt("123");
		$user->save();
    }


    private function seedRevisiones(){
    	DB::table('revisiones')->delete();//Borrado de toda la tabla
    	//id	mascota_id	fechaRevision	descripcion
    	$revision = new Revision();
		//$user->id = ;
		$revision->mascota_id = 1;
		$revision->fechaRevision = '2016-05-16';
		$revision->descripcion = "Esto es una revision";
		$revision->save();
    }
}
