<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Mascota extends Model
{
    
    public function getEdad()
	{
	 $fechaFormateada=Carbon::parse($this->fechaNacimiento);
	 return $fechaFormateada->diffInYears(Carbon::now());
	}

	public function revisiones(){
		return $this->hasMany("App\Revision");
	}
}
