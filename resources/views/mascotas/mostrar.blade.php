@extends('layouts.master')

@section('titulo')
	Mostrar
@endsection

@section('contenido')
	<!--foreach( $mascotaSeleccionada as $infoMascota )
	<p>$infoMascota}}</p>
	endforeach-->
	<div class="row">
		<div class="col-sm-3">
			<img src="{{ asset('assets/imagenes/')}}/{{ $mascotaSeleccionada->imagen }}" style="height:250px" class="rounded-circle border border-light"/>
		</div>
		<div class="col-sm-14">
			<h4 class="display-5">{{ $mascotaSeleccionada->nombre }}</h4>
			<h4 class="display-5">{{ $mascotaSeleccionada->raza }} ( {{$mascotaSeleccionada->getEdad() }} ) años</h4>
			<h4 class="display-5">Cliente: </h4>
			<p>{{ $mascotaSeleccionada->cliente }}</p>
			<h4 class="display-5">Historial</h4>
			<p>{{ $mascotaSeleccionada->historial }}</p>
			<h4 class="display-5">
				{{ $mascotaSeleccionada->revisiones->count() }}
				@if ($mascotaSeleccionada->revisiones->count() == 1)
					revision
				@else
					Revisiones	
				@endif
			</h4>
			<p>
				@foreach ($mascotaSeleccionada->revisiones as $revision)
				 	<li>{{ $revision->descripcion }} {{ $revision->fechaRevision }}</li>
				@endforeach 
			</p>
		<a href="{{ url('mascotas/editar/') }}/{{ $mascotaSeleccionada->id }}" type="button" class="btn btn-warning " > Editar </a>
		<a type="button" class="btn btn-light"> Volver al listado</a>
		</div>
	</div>

@endsection

